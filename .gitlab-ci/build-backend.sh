#!/usr/bin/env sh

set -ex

docker pull $TAGGED_IMAGE_TAG_BACKEND || true
docker pull $IMAGE_TAG_BACKEND:develop || true
docker pull $TAGGED_IMAGE_TAG_DATABASE || true
docker pull $IMAGE_TAG_DATABASE:develop || true

docker build \
    --cache-from $TAGGED_IMAGE_TAG_BACKEND \
    --cache-from $IMAGE_TAG_BACKEND:develop \
    -t $TAGGED_IMAGE_TAG_BACKEND \
    -f .docker/backend/Dockerfile \
    backend

docker build \
    --cache-from $TAGGED_IMAGE_TAG_DATABASE \
    --cache-from $IMAGE_TAG_DATABASE:develop \
    -t $TAGGED_IMAGE_TAG_DATABASE \
    -f .docker/database/Dockerfile \
    backend

docker push $TAGGED_IMAGE_TAG_BACKEND
docker push $TAGGED_IMAGE_TAG_DATABASE
