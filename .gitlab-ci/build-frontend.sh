#!/usr/bin/env sh

set -ex

IMAGE_TAG_FRONTEND_BUILDER="${IMAGE_TAG_FRONTEND}_builder"
TAGGED_IMAGE_TAG_FRONTEND_BUILDER=$IMAGE_TAG_FRONTEND_BUILDER:$CI_COMMIT_REF_SLUG

docker pull $TAGGED_IMAGE_TAG_FRONTEND_BUILDER || true
docker pull $IMAGE_TAG_FRONTEND_BUILDER:develop || true

docker pull $TAGGED_IMAGE_TAG_FRONTEND || true
docker pull $IMAGE_TAG_FRONTEND:develop || true

docker build \
    --cache-from $TAGGED_IMAGE_TAG_FRONTEND_BUILDER \
    --cache-from $IMAGE_TAG_FRONTEND_BUILDER:develop \
    -t $TAGGED_IMAGE_TAG_FRONTEND_BUILDER \
    -f .docker/frontend/Dockerfile_builder \
    .

docker build \
    --cache-from $TAGGED_IMAGE_TAG_FRONTEND \
    --cache-from $IMAGE_TAG_FRONTEND:develop \
    -t $TAGGED_IMAGE_TAG_FRONTEND \
    -f .docker/frontend/Dockerfile \
    --build-arg builder="$TAGGED_IMAGE_TAG_FRONTEND_BUILDER" \
    .

docker push $TAGGED_IMAGE_TAG_FRONTEND_BUILDER
docker push $TAGGED_IMAGE_TAG_FRONTEND
