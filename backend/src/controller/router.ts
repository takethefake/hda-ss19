/** Package imports */
import * as express from 'express';

/** File imports */
import { taskRouter } from './task/task.router';
import { logTime } from '../middleware/timelogger';
import { userRouter } from './user/user.router';

/** Variables */
export const router: express.Router = express.Router({ mergeParams: true });

/** Routes */
router.use('/tasks', logTime, taskRouter);
router.use('/users', userRouter);
