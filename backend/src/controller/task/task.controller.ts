/** Package imports */
import { Response, Request } from 'express';

/** File imports */
import { ITaskModel, taskModel } from '../../model/task';

/**
 * Get all tasks
 * @param {Request} req
 * @param {Response} res
 */
export const getAllTasks = async (req: Request, res: Response) => {
  const tasks: ITaskModel[] = await taskModel.find();
  res.send({
    data: tasks,
  });
};

/**
 * Get a single task by id
 * @param {Request} req
 * @param {Response} res
 */
export const getSingleTask = async (req: Request, res: Response) => {
  const foundTask: ITaskModel | null = await taskModel.findById(req.params.taskId);
  res.send({
    status: 'ok',
    data: foundTask,
  });
};

/**
 * Create a new task
 * @param {Request} req
 * @param {Response} res
 */
export const createTask = async (req: Request, res: Response) => {
  const createdTask: ITaskModel = await taskModel.create(req.body);
  res.status(201).send({
    data: createdTask,
  });
};

/**
 * Update a task by id
 * @param {Request} req
 * @param {Response} res
 */
export const updateTask = async (req: Request, res: Response) => {
  const updatedTask: ITaskModel | null = await taskModel.findByIdAndUpdate(
    req.params.taskId,
    req.body,
    {
      new: true,
    },
  );
  res.send({
    data: updatedTask,
  });
};

/**
 * Delete a task by id
 * @param {Request} req
 * @param {Response} res
 */
export const deleteTask = async (req: Request, res: Response) => {
  const deleteTask: ITaskModel | null = await taskModel.findByIdAndDelete(
    req.params.taskId,
  );
  res.send({
    data: deleteTask,
  });
};
