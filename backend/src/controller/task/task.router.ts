/** Package imports */
import * as express from 'express';

/** File imports */
import { getAllTasks, getSingleTask, createTask, deleteTask, updateTask } from './task.controller';
import { wrapAsync } from '../../middleware/errorhandler';

/** Variables */
export const taskRouter: express.Router = express.Router({ mergeParams: true });

/** Routes */
taskRouter.get('/', wrapAsync(getAllTasks));
taskRouter.get('/:taskId', wrapAsync(getSingleTask));
taskRouter.post('/', wrapAsync(createTask));
taskRouter.delete('/:taskId', wrapAsync(deleteTask));
taskRouter.put('/:taskId', wrapAsync(updateTask));
