/** Package imports */
import { Request, Response } from 'express';
import requestPromise from 'request-promise';

/**
 * Get all users
 * - Filters the received user list to only contain name and website
 * @param {Request} req
 * @param {Response} res
 */
export const getUsers = async (req: Request, res: Response) => {
  const requestOptions: requestPromise.Options = {
    method: 'GET',
    uri: 'https://jsonplaceholder.typicode.com/users',
    json: true,
  };

  const users = await requestPromise(requestOptions);
  const selectedUsers = [];
  for (const user of users) {
    selectedUsers.push({
      name: user.name,
      website: user.website,
    });
  }

  res.send({ status: 'ok', data: selectedUsers });
};
