/** Package imports */
import * as express from 'express';
import { getUsers } from './user.controller';

/** File imports */
import { wrapAsync } from '../../middleware/errorhandler';

/** Variables */
export const userRouter: express.Router = express.Router({ mergeParams: true });

/** Routes */
userRouter.get('/', wrapAsync(getUsers));
