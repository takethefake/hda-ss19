/** Package imports */
import { Request, Response, NextFunction } from 'express';

/** Middleware to log current time */
export const logTime = (req: Request, res: Response, next: NextFunction) => {
  console.info('Time', Date.now());
  next();
};
