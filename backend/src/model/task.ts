/** Package imports */
import { Document, Schema, Model, model } from 'mongoose';

/** Taskmodel interface */
export interface ITaskModel extends Document {
  title: string;
  description: string;
  priority: number;
  done: boolean;
}

/** Task Schema */
export const taskSchema: Schema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: false,
    },
    priority: {
      type: Number,
      required: false,
      default: 0,
    },
    done: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  { timestamps: true },
);

/** Export taskModel */
export const taskModel: Model<ITaskModel> = model<ITaskModel>('Task', taskSchema);
