/** Package imports */
import * as bodyParser from "body-parser";
import config from "config";
import express from "express";
import mongoose from "mongoose";
import cors from "cors";

/** File imports */
import { router } from "./controller/router";
import { globalErrorHandler } from "./middleware/errorhandler";

/** Variables */
export const app: express.Application = express();
app.use(cors());
/** Global middleware */
app.use(bodyParser.json());

/** Setup Database */
mongoose.connect(config.get("database.host"), { useNewUrlParser: true });

/** Routes */
app.use("/api", router);

/** Global error handler */
app.use(globalErrorHandler);

/** Start server */
app.listen(config.get("server.port"), () => {
  console.log("Server is running...");
});
