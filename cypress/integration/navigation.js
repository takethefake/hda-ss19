describe("Navigation", () => {
  it("initial page is tasks", () => {
    //Arrange
    //Act
    cy.visit("/");
    //Assert
    cy.url().should("contain", "/tasks");
  });

  it("has 2 elements in navigation", () => {
    //Arrange
    cy.visit("/");
    //Act
    const lis = cy.get("nav ul").children();
    //Assert
    lis.should("have.length", 2);
  });

  it("can visit dashboard page", () => {
    //Arrange
    cy.visit("/");
    const dashboardLink = cy.get("nav ul").contains("Dashboard");
    //Act
    dashboardLink.click();
    //Assert
    cy.url().should("contain", "/dashboard");
    cy.get("h1").should("contain", "Dashboard");
  });
});
