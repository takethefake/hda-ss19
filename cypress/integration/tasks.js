beforeEach(() => {
  cy.task("deleteDb");
});

describe("Task Page", () => {
  it("can create a todo item", () => {
    cy.visit("/");
    cy.getByLabelText(/title/i).type("Title");
    cy.getByLabelText(/description/i).type("Description");
    cy.getByText(/create/i).click();
    cy.get("[data-cy=task-item]").should("have.length", 1);
  });

  it("can toggle todo", () => {
    //Arrange
    const title = "title";
    const description = "description";
    const priority = 5;
    cy.createTodo(title, description, priority);
    cy.createTodo(title, description, priority);
    cy.createTodo(title, description, priority);
    cy.createTodo(title, description, priority);
    cy.visit("/");
    //Act
    cy.get("[data-cy=task-item]")
      .first()
      .find("input[type='checkbox']")
      .click();

    //Assert
    cy.get("[data-cy=task-item]")
      .first()
      .find("input[type='checkbox']")
      .should("have.checked", true);
  });
});
