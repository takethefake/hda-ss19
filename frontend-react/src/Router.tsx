import * as React from "react";
import { BrowserRouter, Link, Route, Redirect, Switch } from "react-router-dom";
import styled from "styled-components";
import { DashboardPage } from "./pages/DashboardPage";
import { TaskPage } from "./pages/TaskPage";
import { TaskContainer } from "./TaskContainer";

const Nav = styled.nav`
  width: 100%;
  height: 40px;
  background: lightgreen;
`;

const NavItem = styled.li`
  display: inline-block;
  list-style-type: none;
  margin-right: -4px;
  padding: 10px;
  height: 100%;

  & a {
    text-decoration: none;
  }

  &:hover {
    background: green;
  }
`;
const Main = styled.main`
  width: 32rem;
  max-width: 100%;
  margin: 0 auto;
  padding: 0.5rem;
`;

export const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <TaskContainer>
        <Nav>
          <ul>
            <NavItem>
              <Link to="/dashboard">Dashboard</Link>
            </NavItem>
            <NavItem>
              <Link to="/tasks">Tasks</Link>
            </NavItem>
          </ul>
        </Nav>
        <Main>
          <Switch>
            <Route path="/dashboard" component={DashboardPage} />
            <Route path="/tasks" component={TaskPage} />
            <Route render={() => <Redirect to="/tasks" />} />
          </Switch>
        </Main>
      </TaskContainer>
    </BrowserRouter>
  );
};
