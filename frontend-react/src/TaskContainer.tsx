import * as React from "react";
import { Task } from "./pages/TaskPage/components/Task";
import { fetchTasks, editTask, createTask, deleteTask } from "./api";
import { sortByPriority } from "./transformers";
import { TaskToCreate } from "./pages/TaskPage/components/CreateTask";

interface IContext {
  tasks: Task[];
  actions?: {
    handleCheckTask: (taskId: string) => Promise<Task>;
    handleCreateTask: (task: TaskToCreate) => Promise<Task>;
    handleDeleteTask: (taskId: string) => Promise<Response>;
    fetch: () => Promise<Task[]>;
  };
}

const initialState = {
  tasks: []
};

export const taskCtx = React.createContext<IContext>(initialState);

export const TaskContainer: React.FC = ({ children }) => {
  const { Provider } = taskCtx;

  const [tasks, setTasks] = React.useState<Task[]>([]);

  const refetch = (): Promise<Task[]> =>
    new Promise(async (resolve, reject) => {
      try {
        const data = await fetchTasks().then(tasks =>
          tasks.sort(sortByPriority)
        );
        setTasks(data);
        resolve(data);
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });

  const handleCheckTask = (taskId: string): Promise<Task> =>
    new Promise(async (resolve, reject) => {
      const taskIndex = tasks.findIndex((t: any) => t.id === taskId);
      if (taskIndex === -1) {
        return;
      }

      const task = tasks[taskIndex];
      const checkedTask = { ...task, done: !task.done };
      try {
        const responseTask = await editTask(checkedTask);
        const updatedTaskList = [...tasks];
        updatedTaskList.splice(taskIndex, 1, responseTask);
        setTasks(updatedTaskList.sort(sortByPriority));
        resolve(responseTask);
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });

  const handleCreateTask = (task: TaskToCreate): Promise<Task> =>
    new Promise(async (resolve, reject) => {
      const newTask = { ...task, done: false };

      try {
        const responseTask = await createTask(newTask);
        const updatedTaskList = [...tasks, responseTask];
        setTasks(updatedTaskList.sort(sortByPriority));
        resolve(responseTask);
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });

  const handleDeleteTask = (taskId: string): Promise<Response> =>
    new Promise(async (resolve, reject) => {
      const taskIndex = tasks.findIndex((t: any) => t.id === taskId);
      if (taskIndex === -1) {
        return;
      }
      const task = tasks[taskIndex];
      try {
        (async () => {
          const updatedTaskList = [...tasks];
          const response = await deleteTask(task);
          updatedTaskList.splice(taskIndex, 1);
          setTasks(updatedTaskList);
          resolve(response);
        })();
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });

  React.useEffect(() => {
    (async () => {
      await refetch();
    })();
  }, []);

  return (
    <Provider
      value={{
        tasks,
        actions: {
          fetch: refetch,
          handleCheckTask,
          handleCreateTask,
          handleDeleteTask
        }
      }}
    >
      {children}
    </Provider>
  );
};
