import { Task } from "./pages/TaskPage/components/Task";
import { TaskToCreate } from "./pages/TaskPage/components/CreateTask";
import { transformTaskData, sortByPriority } from "./transformers";
export const options = {
  headers: {
    accept: "application/json",
    "Content-Type": "application/json"
  }
};

export const editTask = (task: Task) =>
  new Promise<Task>(async (resolve, reject) => {
    try {
      const transformedTask = await fetch(`/api/tasks/${task.id}`, {
        ...options,
        method: "PUT",
        body: JSON.stringify(task)
      })
        .then(res => res.json())
        .then(({ data }) => transformTaskData(data));
      resolve(transformedTask);
    } catch (e) {
      reject(e);
    }
  });

export const createTask = (task: TaskToCreate) =>
  new Promise<Task>(async (resolve, reject) => {
    try {
      const transformedTask = await fetch(`/api/tasks`, {
        ...options,
        method: "POST",
        body: JSON.stringify(task)
      })
        .then(res => res.json())
        .then(({ data }) => transformTaskData(data));
      resolve(transformedTask);
    } catch (e) {
      reject(e);
    }
  });

export const deleteTask = (task: Task) =>
  new Promise<Response>(async (resolve, reject) => {
    try {
      const response = await fetch(`/api/tasks/${task.id}`, {
        ...options,
        method: "DELETE"
      });
      resolve(response);
    } catch (e) {
      reject(e);
    }
  });

export const fetchTasks = () =>
  new Promise<Task[]>(async (resolve, reject) => {
    try {
      const response = await fetch(`/api/tasks`, {
        ...options,
        method: "GET"
      })
        .then(res => res.json())
        .then(({ data }) => data.map(transformTaskData));
      resolve(response);
    } catch (e) {
      reject(e);
    }
  });
