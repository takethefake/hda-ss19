import * as React from "react";
import { taskCtx } from "../TaskContainer";

export const DashboardPage: React.FC = () => {
  const context = React.useContext(taskCtx);
  return (
    <>
      <h1>Dashboard Page</h1>
      <p>{`You have ${context.tasks.length} tasks`}</p>
      <button onClick={() => context.actions!.fetch()}>refetch</button>
    </>
  );
};
