import React from "react";
import ReactDOM from "react-dom";
import { TaskPage } from ".";
import { TaskContainer } from "../../TaskContainer";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <TaskContainer>
      <TaskPage />
    </TaskContainer>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
