import * as React from "react";
import { useState } from "react";
import styled from "styled-components";

const CreateTaskForm = styled.form`
  margin-bottom: 1.5rem;
  padding-bottom: 1rem;
  border-bottom: 1px solid rgba(0, 0, 0, 0.3);
`;

const FormItem = styled.div`
  margin-bottom: 1rem;
  label {
    display: block;
    font-weight: 600;
    padding-bottom: 0.3em;
  }

  input,
  textarea {
    width: 100%;
    max-width: 100%;
    font-size: 1rem;
  }
`;

const CreateTaskButton = styled.button`
  all: unset;
  box-sizing: border-box;
  color: #b4d455;
  border: 1px solid currentColor;
  border-radius: 3px;
  padding: 0.3em 0.5em;
  width: 100%;
  text-align: center;
  cursor: pointer;
  transition: color 200ms ease-in-out;

  &:hover {
    color: #5f7e3f;
  }
`;

const ErrorAlert = styled.p`
  color: darkred;
`;

export type TaskToCreate = {
  title: string;
  description: string;
  priority: number;
};

export type CreateTaskProps = {
  onCreateTask: (task: TaskToCreate) => void;
};

export const CreateTask: React.FC<CreateTaskProps> = ({ onCreateTask }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [priority, setPriority] = useState("5");
  const [errorMessage, setErrorMessage] = useState("");

  function handleCreateTask(e: React.SyntheticEvent) {
    e.preventDefault();
    if (!title) {
      setErrorMessage("A title is required");
      return;
    }
    onCreateTask({ title, description, priority: parseInt(priority) });
    handleFormReset(e);
  }

  function handleFormReset(e: React.SyntheticEvent) {
    e.preventDefault();
    setTitle("");
    setDescription("");
    setPriority("5");
    setErrorMessage("");
  }

  return (
    <CreateTaskForm onSubmit={handleCreateTask} onReset={handleFormReset}>
      <FormItem>
        <label htmlFor="create-task-title">Title</label>
        <input
          id="create-task-title"
          type="text"
          value={title}
          onChange={e => setTitle(e.target.value)}
        />
      </FormItem>

      <FormItem>
        <label htmlFor="create-task-description">Description</label>
        <textarea
          id="create-task-description"
          value={description}
          onChange={e => setDescription(e.target.value)}
        />
      </FormItem>

      <FormItem>
        <label htmlFor="create-task-priority">Priority</label>
        <input
          id="create-task-priority"
          type="number"
          min={0}
          max={10}
          value={priority}
          onChange={e => setPriority(e.target.value)}
        />
      </FormItem>

      <CreateTaskButton type="submit">Create</CreateTaskButton>
      {errorMessage && <ErrorAlert>{errorMessage}</ErrorAlert>}
    </CreateTaskForm>
  );
};
