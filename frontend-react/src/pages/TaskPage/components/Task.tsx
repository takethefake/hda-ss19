import * as React from "react";
import styled from "styled-components";

const TaskItem = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 1rem;
  box-shadow: 0 3px 10px rgba(0, 0, 0, 0.3);
  margin-bottom: 0.75rem;
`;

const Checkbox = styled.input`
  margin: 0.5em 1rem 0.5em 0.5em;
`;

const TaskTitle = styled.span`
  font-size: 1.3rem;
  font-weight: 600;
`;

const TaskDescription = styled.p`
  white-space: pre-wrap;
`;

const Actions = styled.div`
  flex: 1 0 100%;
  text-align: right;
`;

const DeleteButton = styled.button`
  all: unset;
  box-sizing: border-box;
  display: inline-block;
  color: darkred;
  transition: color 200ms ease-in-out;
  cursor: pointer;

  &:hover {
    color: red;
  }
`;

export type Task = {
  id: string;
  title: string;
  description: string;
  done: boolean;
  priority: number;
};

export type TaskProps = {
  task: Task;
  onCheck: () => void;
  onDelete: () => void;
};

export const TaskCard: React.FC<TaskProps> = ({
  task: { description, done, title },
  onCheck,
  onDelete
}) => (
  <TaskItem data-cy="task-item">
    <Actions>
      <DeleteButton onClick={onDelete}>Delete</DeleteButton>
    </Actions>
    <div>
      <Checkbox type="checkbox" checked={done} onChange={onCheck} />
    </div>
    <div>
      <TaskTitle>{title}</TaskTitle>
      <TaskDescription>{description}</TaskDescription>
    </div>
  </TaskItem>
);
