import * as React from "react";
import styled from "styled-components";
import { Task, TaskCard } from "./Task";

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;

type TaskListProps = {
  tasks: Task[];
  onCheckTask: (taskId: string) => void;
  onDeleteTask: (taskId: string) => void;
};

export const TaskList: React.FC<TaskListProps> = ({
  tasks,
  onCheckTask,
  onDeleteTask
}) => {
  return (
    <List>
      {tasks.length === 0 ? (
        <p>No tasks due - enjoy your day</p>
      ) : (
        tasks.map(task => {
          const onCheck = () => onCheckTask(task.id);
          const onDelete = () => onDeleteTask(task.id);
          return (
            <li key={task.id}>
              <TaskCard task={task} onCheck={onCheck} onDelete={onDelete} />
            </li>
          );
        })
      )}
    </List>
  );
};
