import * as React from "react";
import { TaskList } from "./components/TaskList";
import styled from "styled-components";
import { CreateTask, TaskToCreate } from "./components/CreateTask";
import { Task } from "./components/Task";
import { editTask, createTask, deleteTask, fetchTasks } from "../../api";
import { sortByPriority } from "../../transformers";
import { taskCtx } from "../../TaskContainer";
1;
const Heading = styled.h1`
  margin: 0.5rem auto 1rem;
`;

export const TaskPage: React.FC = () => {
  const { tasks, actions } = React.useContext(taskCtx);

  return (
    <>
      <Heading>Tasks</Heading>
      <CreateTask onCreateTask={actions!.handleCreateTask} />
      <TaskList
        tasks={tasks}
        onCheckTask={actions!.handleCheckTask}
        onDeleteTask={actions!.handleDeleteTask}
      />
    </>
  );
};
