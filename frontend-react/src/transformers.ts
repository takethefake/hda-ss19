import { Task } from "./pages/TaskPage/components/Task";
export const transformTaskData = (data: any): Task => {
  const { _id, title, description, done, priority } = data;
  return { id: _id, title, description, done, priority };
};

export function sortByPriority(a: Task, b: Task) {
  return a.priority === b.priority ? 0 : a.priority < b.priority ? -1 : 1;
}
